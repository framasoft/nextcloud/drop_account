<?php

/**
 * SPDX-FileCopyrightText: 2017 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Settings;

use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Service\DeletionCapabilityService;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\IAppConfig;
use OCP\IUserSession;
use OCP\Settings\ISettings;
use OCP\Util;

class Personal implements ISettings {

	public function __construct(
		private IAppConfig $appConfig,
		private IUserSession $userSession,
		private DeletionCapabilityService $deletionCapabilityService,
		private IInitialState $initialState,
	) {
	}

	/**
	 * @return TemplateResponse returns the instance with all parameters set, ready to be rendered
	 * @since 9.1
	 */
	public function getForm(): TemplateResponse {
		$user = $this->userSession->getUser();
		if ($user) {
			[$canDeleteAccount, $reason] = $this->deletionCapabilityService->canUserBeDeleted($user);
			$requiresConfirmation = $this->appConfig->getValueString(Application::APP_NAME, 'requireConfirmation', 'no') === 'yes';
			$hasEmailForConfirmation = $requiresConfirmation && !($user->getEMailAddress() === null || $user->getEMailAddress() === '');
			$willDelayPurge = $this->appConfig->getValueString(Application::APP_NAME, 'delayPurge', 'no') === 'yes';
			$delayPurgeHours = (int)$this->appConfig->getValueString(Application::APP_NAME, 'delayPurgeHours', '24');

			Util::addScript(Application::APP_NAME, 'drop_account-personal-settings');
			Util::addStyle(Application::APP_NAME, 'drop_account-personal-settings');
			$this->initialState->provideInitialState('has_email_for_confirmation', $hasEmailForConfirmation);
			$this->initialState->provideInitialState('can_delete_account', $canDeleteAccount);
			$this->initialState->provideInitialState('deletion_impossible_reason', $reason ?? false);
			$this->initialState->provideInitialState('will_delay_purge', $willDelayPurge);
			$this->initialState->provideInitialState('delay_purge_hours', $delayPurgeHours);
			$this->initialState->provideInitialState('require_confirmation', $requiresConfirmation);
		}

		return new TemplateResponse(Application::APP_NAME, 'personal');
	}

	/**
	 * @return string the section ID, e.g. 'sharing'
	 * @since 9.1
	 * @psalm-return 'drop_account'
	 */
	public function getSection(): string {
		return Application::APP_NAME;
	}

	/**
	 * @return int whether the form should be rather on the top or bottom of
	 *             the admin section. The forms are arranged in ascending order of the
	 *             priority values. It is required to return a value between 0 and 100.
	 *
	 * E.g.: 70
	 * @since 9.1
	 * @psalm-return 40
	 */
	public function getPriority(): int {
		return 40;
	}
}
