<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Service;

use DateInterval;
use DateTime;
use Exception;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\MissingEmailException;
use OCP\Defaults;
use OCP\IAppConfig;
use OCP\IDateTimeFormatter;
use OCP\IL10N;
use OCP\IURLGenerator;
use OCP\IUser;
use OCP\L10N\IFactory;
use OCP\Mail\IEMailTemplate;
use OCP\Mail\IMailer;
use OCP\Mail\IMessage;
use OCP\Util;

class MailerService {

	private IL10N $l10n;

	public function __construct(
		private IAppConfig $appConfig,
		private IFactory $l10nFactory,
		private IMailer $mailer,
		private Defaults $defaults,
		private IURLGenerator $urlGenerator,
		private IDateTimeFormatter $dateFormatter,
	) {
		$this->l10n = $l10nFactory->get(Application::APP_NAME);
	}

	/**
	 * @param IUser $user
	 * @throws MissingEmailException
	 * @throws Exception
	 */
	public function sendFinalEmail(IUser $user): void {
		$to = $this->getRecipients($user);
		$template = $this->createFinalTemplate();
		$message = $this->createMessage($to, $template);
		$this->mailer->send($message);
	}

	/**
	 * @throws MissingEmailException
	 * @throws Exception
	 */
	public function sendReactivationEmail(IUser $user): void {
		$to = $this->getRecipients($user);
		$template = $this->createReactivationTemplate($user);
		$message = $this->createMessage($to, $template);
		$this->mailer->send($message);
	}

	/**
	 * @param array $recipients
	 * @param IEMailTemplate $template
	 * @return IMessage
	 */
	private function createMessage(array $recipients, IEMailTemplate $template): IMessage {
		return $this->mailer->createMessage()
			->setFrom([Util::getDefaultEmailAddress('noreply') => $this->defaults->getName()])
			->setTo($recipients)
			->useTemplate($template);
	}

	/**
	 * @throws MissingEmailException
	 */
	private function getRecipients(IUser $user): array {
		$recipientName = $user->getDisplayName();
		$recipient = $user->getEMailAddress();
		if (is_string($recipient) && $recipientName) {
			return [$recipient => $recipientName];
		}

		if (is_string($recipient)) {
			return [$recipient];
		}
		throw new MissingEmailException();
	}

	/**
	 * @return IEMailTemplate
	 * @throws Exception
	 */
	private function createFinalTemplate(): IEMailTemplate {
		$scheduled = $this->appConfig->getValueString(Application::APP_NAME, 'delayPurge', 'no') === 'yes';
		$purgeHours = (int)$this->appConfig->getValueString(Application::APP_NAME, 'delayPurgeHours', '24');
		$purgeDays = intdiv($purgeHours, 24);

		$expirationDate = new DateTime();
		$duration = 'PT' . $purgeHours . 'H';
		$expirationDate->add(new DateInterval($duration));
		$formattedDate = $this->dateFormatter->formatDate($expirationDate);

		$serverName = $this->defaults->getName();
		$template = $this->mailer->createEMailTemplate('drop_account.deletion');

		if ($scheduled) {
			$template->setSubject($this->l10n->t('Your account on %s has been disabled', [$serverName]));
		} else {
			$template->setSubject($this->l10n->t('Your account on %s has been deleted', [$serverName]));
		}
		$template->addHeader();
		if ($scheduled) {
			$template->addHeading($this->l10n->t('Account deletion on %s scheduled', [$serverName]));
		} else {
			$template->addHeading($this->l10n->t('Account on %s deleted', [$serverName]));
		}
		$template->addBodyText($this->l10n->t('Hi,'));
		if ($scheduled) {
			if ($purgeDays === 0) {
				$template->addBodyText($this->l10n->n('Your data will be permanently deleted in %n hour.', 'Your data will be permanently deleted in %n hours.', $purgeHours, [$purgeHours]));
			} else {
				$template->addBodyText($this->l10n->n('Your data will be permanently deleted in %n day, on %s.', 'Your data will be permanently deleted in %n days, on %s.', $purgeDays, [$formattedDate]));
			}

			$template->addBodyText($this->l10n->t('You can cancel your account deletion by contacting an administrator before the %s.', [$formattedDate]));
		} else {
			$template->addBodyText($this->l10n->t('We confirm all your personal data on %s was permanently deleted.', [$serverName]));
		}
		$template->addBodyText($this->l10n->t('Thanks for using our service.'));
		$template->addBodyText($this->l10n->t('Cheers!'));
		$template->addFooter();
		return $template;
	}

	private function createReactivationTemplate(IUser $user): IEMailTemplate {
		// Since the user is being reactivated, current user isn't $user, and language can be different, so we don't use I10N directly
		$lang = $this->l10nFactory->getUserLanguage($user);
		$l10n = $this->l10nFactory->get(Application::APP_NAME, $lang);
		$serverName = $this->defaults->getName();
		$template = $this->mailer->createEMailTemplate('drop_account.reactivation');
		$template->setSubject($l10n->t('Your account on %s has been reactivated', [$serverName]));
		$template->addHeader();
		$template->addHeading($l10n->t('Access to your account %1$s has been restored', [$user->getUID()]));
		$template->addBodyText($l10n->t('An administrator re-enabled your account on %s before it was destroyed.', [$serverName]));
		$template->addBodyText($l10n->t('Your account is available again, none of your data was removed. You may now login again.'));
		$template->addBodyButton($l10n->t('Login'), $this->urlGenerator->getBaseUrl());
		return $template;
	}
}
