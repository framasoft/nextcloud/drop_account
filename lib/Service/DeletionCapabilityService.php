<?php

declare(strict_types=1);

/**
 * SPDX-FileCopyrightText: 2023 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Service;

use OCP\IGroupManager;
use OCP\IL10N;
use OCP\IUser;
use OCP\IUserManager;

class DeletionCapabilityService {

	public function __construct(
		private IUserManager $userManager,
		private IGroupManager $groupManager,
		private IL10N $l10n,
	) {
	}

	/**
	 * @return list{boolean, string|null}
	 */
	public function canUserBeDeleted(IUser $user): array {
		if (!$this->userBackendAllowsDeletion($user)) {
			return [false, $this->l10n->t('User backend configuration does not allow deletion')];
		}
		if ($this->isOnlyUser()) {
			return [false, $this->l10n->t("You are the only user on the server, you can't delete your account.")];
		}
		if ($this->isOnlyAdmin($user)) {
			return [false, $this->l10n->t("You are the only admin on the server, you can't delete your account.")];
		}
		return [true, null];
	}

	/**
	 * Currently we only allow Database and Guests users to be deleted
	 */
	private function userBackendAllowsDeletion(IUser $user): bool {
		return in_array($user->getBackendClassName(), ['Database', 'Guests']);
	}

	private function isOnlyUser(): bool {
		return $this->userManager->countUsers() < 2;
	}

	private function isOnlyAdmin(IUser $user): bool {
		$adminGroup = $this->groupManager->get('admin');
		return $adminGroup && $adminGroup->count() < 2 && $this->groupManager->isAdmin($user->getUID());
	}
}
