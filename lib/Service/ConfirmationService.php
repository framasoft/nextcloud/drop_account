<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Service;

use Exception;
use OCA\DropAccount\MissingEmailException;
use OCP\Defaults;
use OCP\IL10N;
use OCP\IURLGenerator;
use OCP\IUser;
use OCP\Mail\IEMailTemplate;
use OCP\Mail\IMailer;
use OCP\Mail\IMessage;
use OCP\Security\ISecureRandom;
use OCP\Util;

class ConfirmationService {

	public function __construct(
		private IL10N $l10n,
		private ISecureRandom $random,
		private IMailer $mailer,
		private Defaults $defaults,
		private IURLGenerator $urlGenerator,
	) {

	}

	/**
	 * @param IUser $user
	 * @return string
	 * @throws MissingEmailException
	 * @throws Exception
	 */
	public function sendConfirmationEmail(IUser $user): string {
		$recipientName = $user->getDisplayName();
		$recipient = $user->getEMailAddress();
		if (is_string($recipient) && $recipientName) {
			$to = [$recipient => $recipientName];
		} elseif (is_string($recipient)) {
			$to = [$recipient];
		} else {
			throw new MissingEmailException();
		}


		$token = $this->random->generate(16, ISecureRandom::CHAR_HUMAN_READABLE);
		$template = $this->createTemplate($token);
		$message = $this->createMessage($to, $template);
		$this->mailer->send($message);
		return $token;
	}

	/**
	 * @param array $recipients
	 * @param IEMailTemplate $template
	 * @return IMessage
	 */
	private function createMessage(array $recipients, IEMailTemplate $template): IMessage {
		return $this->mailer->createMessage()
			->setFrom([Util::getDefaultEmailAddress('noreply') => $this->defaults->getName()])
			->setTo($recipients)
			->useTemplate($template);
	}

	/**
	 * @param string $token
	 * @return IEMailTemplate
	 */
	private function createTemplate(string $token): IEMailTemplate {
		$serverName = $this->defaults->getName();
		$template = $this->mailer->createEMailTemplate('drop_account.delete_confirmation');
		$template->setSubject($this->l10n->t('Confirm your account deletion on %s', [$serverName]));
		$template->addHeader();
		$template->addHeading($this->l10n->t('Account deletion confirmation for %s', [$serverName]));
		$template->addBodyText($this->l10n->t('Hello,'));
		$template->addBodyText($this->l10n->t('Someone - probably you - asked to delete their account on %s.', [$serverName]));
		$template->addBodyText($this->l10n->t('To confirm the account deletion, you may click on the button below.'));
		$template->addBodyText($this->l10n->t("If you didn't request this email, you should contact your administrator as soon as possible, someone may be accessing your account without your knowledge."));
		$template->addBodyButton($this->l10n->t('Delete account'), $this->getURLFromToken($token));
		$template->addBodyText($this->l10n->t('Cheers!'));
		$template->addFooter();
		return $template;
	}

	/**
	 * Get URL from public sharing token
	 *
	 * @param string $token
	 * @return string
	 */
	private function getURLFromToken(string $token): string {
		return $this->urlGenerator->linkToRouteAbsolute('drop_account.account.confirm', [
			'token' => $token,
		]);
	}
}
