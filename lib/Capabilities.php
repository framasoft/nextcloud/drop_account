<?php

declare(strict_types=1);

/**
 * SPDX-FileCopyrightText: 2023 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount;

use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Service\DeletionCapabilityService;
use OCP\Capabilities\ICapability;
use OCP\IAppConfig;
use OCP\IL10N;
use OCP\IUserSession;

class Capabilities implements ICapability {


	public function __construct(
		private IUserSession $userSession,
		private DeletionCapabilityService $deletionCapabilityService,
		private IAppConfig $config,
		private IL10N $l10n,
	) {
	}

	/**
	 * @return array{drop-account: array{enabled: boolean, api-version: string, delay: array{enabled: bool, hours: int}, details: string|null}}
	 */
	public function getCapabilities(): array {
		[$enabled, $details] = $this->canUserBeDeleted();
		return ['drop-account' =>
			[
				'enabled' => $enabled,
				'delay' => [
					'enabled' => $this->config->getValueString(Application::APP_NAME, 'delayPurge', 'no') === 'yes',
					'hours' => (int)$this->config->getValueString(Application::APP_NAME, 'delayPurgeHours', '24'),
				],
				'details' => $details,
				'api-version' => 'v1',
			]
		];
	}

	/**
	 * @return list{boolean, string|null}
	 */
	private function canUserBeDeleted(): array {
		$user = $this->userSession->getUser();
		if (!$user) {
			return [false, $this->l10n->t('User session not found')];
		}
		return $this->deletionCapabilityService->canUserBeDeleted($user);
	}
}
