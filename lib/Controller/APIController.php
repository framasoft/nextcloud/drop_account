<?php

/**
 * SPDX-FileCopyrightText: 2017 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Controller;

use DateTime;
use Exception;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCA\DropAccount\MissingEmailException;
use OCA\DropAccount\Service\ConfirmationService;
use OCA\DropAccount\Service\DeletionCapabilityService;
use OCA\DropAccount\Service\MailerService;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\OCSController;
use OCP\BackgroundJob\IJobList;
use OCP\IAppConfig;
use OCP\IConfig;
use OCP\IL10N;
use OCP\IRequest;
use OCP\IUser;
use OCP\IUserSession;
use OCP\PreConditionNotMetException;
use Psr\Log\LoggerInterface;

class APIController extends OCSController {

	public function __construct(
		string $appName,
		IRequest $request,
		private IUserSession $userSession,
		private LoggerInterface $logger,
		private IConfig $config,
		private IAppConfig $appConfig,
		private IL10N $l10n,
		private IJobList $jobList,
		private ConfirmationService $confirmationService,
		private MailerService $mailerService,
		private DeletionCapabilityService $deletionCapabilityService,
	) {
		parent::__construct($appName, $request);
	}

	/**
	 *
	 * Trigger the deletion of the current user account
	 *
	 * @NoAdminRequired
	 * @PasswordConfirmationRequired
	 * @return DataResponse<Http::STATUS_CREATED, array{message: string}, array{}>|DataResponse<Http::STATUS_ACCEPTED, array{}, array{}>|DataResponse<Http::STATUS_UNAUTHORIZED, array{message: string}, array{}>|DataResponse<Http::STATUS_FORBIDDEN, array{message: string|null}, array{}>|DataResponse<Http::STATUS_BAD_REQUEST, array{message: string}, array{}>|DataResponse<Http::STATUS_INTERNAL_SERVER_ERROR, array{message: string}, array{}>
	 *                                                                                                                                                                                                                                                                                                                                                                                                                                                 201: A confirmation by email is required. An email has been sent to the user
	 *                                                                                                                                                                                                                                                                                                                                                                                                                                                 202: The user has been marked for deletion
	 *                                                                                                                                                                                                                                                                                                                                                                                                                                                 400: The user has no email set up in it's account to send the confirmation email to
	 *                                                                                                                                                                                                                                                                                                                                                                                                                                                 401: Unable to get user from session
	 *                                                                                                                                                                                                                                                                                                                                                                                                                                                 403: The user was not allowed to be deleted
	 *                                                                                                                                                                                                                                                                                                                                                                                                                                                 500: An exception was thrown when sending the user a confirmation email
	 */
	public function delete(): DataResponse {
		$user = $this->userSession->getUser();

		if (!$user) {
			return new DataResponse(['message' => $this->l10n->t('User-Session unexpectedly expired')], Http::STATUS_UNAUTHORIZED);
		}

		[$canBeDeleted, $reason] = $this->deletionCapabilityService->canUserBeDeleted($user);
		if (!$canBeDeleted) {
			return new DataResponse([
				'message' => $reason
			], Http::STATUS_FORBIDDEN);
		}

		if ($this->appConfig->getValueString(Application::APP_NAME, 'requireConfirmation', 'no') === 'yes') {
			try {
				$token = $this->confirmationService->sendConfirmationEmail($user);
				$this->config->setUserValue($user->getUID(), Application::APP_NAME, 'delete_token', $token);
				return new DataResponse([
					'message' => $this->l10n->t('Successfully sent email')
				], Http::STATUS_CREATED);
			} catch (MissingEmailException) {
				return new DataResponse([
					'message' => $this->l10n->t('You have no email set up into your account')
				], Http::STATUS_BAD_REQUEST);
			} catch (Exception $e) {
				$this->logger->error('Unknown exception when sending email for purging user', ['exception' => $e]);
				return new DataResponse([
					'message' => $this->l10n->t('Unexpected error sending email. Please contact your administrator.')
				], Http::STATUS_INTERNAL_SERVER_ERROR);
			}
		} else {
			$this->performDelete($user);
			return new DataResponse([], Http::STATUS_ACCEPTED);
		}
	}

	/**
	 * @param IUser $user
	 */
	private function performDelete(IUser $user): void {
		$this->config->deleteUserValue($user->getUID(), Application::APP_NAME, 'delete_token');
		try {
			$this->mailerService->sendFinalEmail($user);
		} catch (Exception) {
			// Do nothing, this is not blocking
		}
		$user->setEnabled(false);

		/**
		 * If we delay purge, just set the date when the user should be removed to a config user value
		 */
		if ($this->appConfig->getValueString(Application::APP_NAME, 'delayPurge', 'no') === 'yes') {
			try {
				$this->config->setUserValue($user->getUID(), Application::APP_NAME, 'markedForPurge', 'yes');
				$this->config->setUserValue($user->getUID(), Application::APP_NAME, 'purgeDate', (string)(new DateTime())->getTimestamp());
			} catch (PreConditionNotMetException) {
				// Do nothing, it will not happen, we don't give preconditions
			}
		} else {
			$this->jobList->add(DeleteAccountData::class, ['uid' => $user->getUID()]);
		}
		$this->userSession->logout();
	}
}
