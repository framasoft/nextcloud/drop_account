<?php

/**
 * SPDX-FileCopyrightText: 2017 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Controller;

use DateTime;
use Exception;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCA\DropAccount\Service\ActivityService;
use OCA\DropAccount\Service\MailerService;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\BackgroundJob\IJobList;
use OCP\IAppConfig;
use OCP\IConfig;
use OCP\IRequest;
use OCP\IURLGenerator;
use OCP\IUser;
use OCP\IUserSession;
use OCP\PreConditionNotMetException;

class AccountController extends Controller {

	public function __construct(
		string $appName,
		IRequest $request,
		private IUserSession $userSession,
		private IConfig $config,
		private IAppConfig $appConfig,
		private IJobList $jobList,
		private IURLGenerator $urlGenerator,
		private MailerService $mailerService,
		private ActivityService $activityService,
	) {
		parent::__construct($appName, $request);
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @param string $token
	 * @IgnoreOpenAPI
	 * @return TemplateResponse
	 * @throws PreConditionNotMetException
	 */
	public function confirm(string $token): TemplateResponse {
		$user = $this->userSession->getUser();
		if (!$user) {
			$response = new TemplateResponse($this->appName, 'account-deleted', [
				'uid' => null,
				'status' => 'not-found'
			], 'guest');
			$response->setStatus(Http::STATUS_UNAUTHORIZED);
			return $response;
		}

		$uid = $user->getUID();
		$savedToken = $this->config->getUserValue($uid, Application::APP_NAME, 'delete_token', null);

		if ($savedToken !== $token) {
			$response = new TemplateResponse($this->appName, 'account-deleted', [
				'uid' => $uid,
				'status' => 'invalid-token',
				'rootURL' => $this->urlGenerator->getBaseUrl()
			], 'guest');
			$response->setStatus(Http::STATUS_NOT_FOUND);
			return $response;
		}

		$this->performDelete($user);

		return new TemplateResponse($this->appName, 'account-deleted', [
			'uid' => $uid,
			'status' => 'deleted'
		], 'guest');
	}

	/**
	 * @param IUser $user
	 * @throws PreConditionNotMetException
	 */
	private function performDelete(IUser $user): void {
		$this->config->deleteUserValue($user->getUID(), Application::APP_NAME, 'delete_token');
		try {
			$this->mailerService->sendFinalEmail($user);
		} catch (Exception) {
			// Do nothing, this is not blocking
		}
		$user->setEnabled(false);

		/**
		 * If we delay purge, just set the date when the user should be removed to a config user value
		 */
		if ($this->appConfig->getValueString(Application::APP_NAME, 'delayPurge', 'no') === 'yes') {
			$purgeDate = (new DateTime())->getTimestamp();
			$this->config->setUserValue($user->getUID(), Application::APP_NAME, 'markedForPurge', 'yes');
			$this->config->setUserValue($user->getUID(), Application::APP_NAME, 'purgeDate', (string)$purgeDate);
			$this->activityService->createAdminActivities($user, ['subject' => 'account_scheduled_self_deletion', 'subject_params' => ['expiration' => $purgeDate]]);
			$this->activityService->sendActivities();
		} else {
			$this->jobList->add(DeleteAccountData::class, ['uid' => $user->getUID()]);
		}
		$this->userSession->logout();
	}
}
