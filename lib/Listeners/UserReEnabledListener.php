<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Listeners;

use Exception;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCA\DropAccount\Service\ActivityService;
use OCA\DropAccount\Service\MailerService;
use OCP\BackgroundJob\IJobList;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\IConfig;
use OCP\User\Events\UserChangedEvent;
use Psr\Log\LoggerInterface;

/**
 * @template-implements IEventListener<Event|UserChangedEvent>
 */
class UserReEnabledListener implements IEventListener {

	public function __construct(
		private IJobList $jobList,
		private IConfig $config,
		private MailerService $mailerService,
		private ActivityService $activityService,
		private LoggerInterface $logger,
	) {
	}

	public function handle(Event $event): void {
		$this->logger->debug('Handle event in UserReEnabledListener', ['event' => $event]);
		if ($event instanceof UserChangedEvent) {
			$user = $event->getUser();
			$uid = $user->getUID();
			/** @var mixed $feature */
			$feature = $event->getFeature();
			/** @var mixed $oldValue */
			$oldValue = $event->getOldValue();
			/** @var mixed $value */
			$value = $event->getValue();

			// We only do things on status being enabled
			if ($feature === 'enabled' && $value === true && $oldValue === false) {
				$this->logger->info('Re-enabling an user', ['event' => $event]);
				// Clear dedicated job if we have one
				if ($this->jobList->has(DeleteAccountData::class, ['uid' => $uid])) {
					$this->jobList->remove(DeleteAccountData::class, ['uid' => $uid]);
				}
				if ($this->config->getUserValue($uid, Application::APP_NAME, 'markedForPurge') === 'yes') {
					try {
						$this->mailerService->sendReactivationEmail($user);
						$this->activityService->createAdminActivities($user, ['subject' => 'account_re_enabled']);
						$this->activityService->sendActivities();
					} catch (Exception) {
						// Nothing to do, there can be issues with sending email, but we don't care
					}
				}
				// Always clear purge values anyway
				$this->config->deleteUserValue($uid, Application::APP_NAME, 'markedForPurge');
				$this->config->deleteUserValue($uid, Application::APP_NAME, 'purgeDate');
			}
		}
	}
}
