<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\BackgroundJob;

use ChristophWurst\Nextcloud\Testing\TestCase;
use DateTime;
use Exception;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\TimedDeleteAccountData;
use OCA\DropAccount\Service\DeleteAccountDataService;
use OCP\AppFramework\Utility\ITimeFactory;
use OCP\IAppConfig;
use OCP\IConfig;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

class TimedDeleteAccountDataTest extends TestCase {
	private TimedDeleteAccountData $timedDeleteAccountDataJob;
	/**
	 * @var DeleteAccountDataService|MockObject
	 */
	private $service;

	/** @var MockObject|LoggerInterface|(LoggerInterface&MockObject) */
	private $logger;

	/**
	 * @var IConfig|MockObject
	 */
	private $config;

	/**
	 * @var IAppConfig|MockObject
	 */
	private $appConfig;

	public function setUp(): void {
		parent::setUp();
		$timeFactory = $this->createMock(ITimeFactory::class);
		$this->config = $this->createMock(IConfig::class);
		$this->appConfig = $this->createMock(IAppConfig::class);
		$this->service = $this->createMock(DeleteAccountDataService::class);
		$this->logger = $this->createMock(LoggerInterface::class);
		$this->timedDeleteAccountDataJob = new TimedDeleteAccountData($timeFactory, $this->config, $this->appConfig, $this->service, $this->logger);
	}

	/**
	 * @throws Exception
	 */
	public function test(): void {
		$config = [['uid' => 'uid1', 'date' => null], ['uid' => 'uid2', 'date' => (string)(new DateTime())->getTimestamp()]];
		$configMap = array_map(static function ($key) {
			return array_merge([$key['uid']], [Application::APP_NAME, 'purgeDate', null]);
		}, $config);
		$uids = array_map(static function ($key) {
			return $key['uid'];
		}, $config);
		$dates = array_map(static function ($key) {
			return $key['date'];
		}, $config);

		$this->config->expects($this->once())->method('getUsersForUserValue')->with(Application::APP_NAME, 'markedForPurge', 'yes')->willReturn($uids);
		$this->appConfig->expects($this->once())->method('getValueString')->with(Application::APP_NAME, 'delayPurgeHours', '24')->willReturn('24');

		$matcher = $this->exactly(count($uids));
		$this->config->expects($matcher)->method('getUserValue')->willReturnCallback(function ($uid, $appId, $key, $default) use ($matcher, $configMap, $dates): ?string {
			$this->assertEquals($configMap[$matcher->numberOfInvocations() - 1][0], $uid);
			$this->assertEquals($configMap[$matcher->numberOfInvocations() - 1][1], $appId);
			$this->assertEquals($configMap[$matcher->numberOfInvocations() - 1][2], $key);
			$this->assertEquals($configMap[$matcher->numberOfInvocations() - 1][3], $default);
			return $dates[$matcher->numberOfInvocations() - 1];
		});
		$this->logger->expects($this->once())->method('info');
		$this->service->expects($this->atMost(1))->method('delete')->with($uids[0]);

		$this->timedDeleteAccountDataJob->run([]);
	}
}
