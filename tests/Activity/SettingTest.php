<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Activity;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\Activity\Setting;
use OCP\IL10N;

class SettingTest extends TestCase {
	private Setting $setting;

	public function setUp(): void {
		parent::setUp();
		$l10n = $this->createMock(IL10N::class);
		$l10n
			->method('t')
			->willReturnCallback(function ($string, $args) {
				return 'TRANSLATED: ' . vsprintf($string, $args);
			});
		$this->setting = new Setting($l10n);
	}

	public function testGetIdentifier(): void {
		$this->assertEquals('account_deletion', $this->setting->getIdentifier());
	}

	public function testGetName(): void {
		$this->assertEquals('TRANSLATED: A user <strong>deleted</strong> its account', $this->setting->getName());
	}

	public function testGetPriority(): void {
		$this->assertEquals(80, $this->setting->getPriority());
	}

	public function testCanChangeStream(): void {
		$this->assertTrue($this->setting->canChangeStream());
	}

	public function testIsDefaultEnabledStream(): void {
		$this->assertTrue($this->setting->isDefaultEnabledStream());
	}

	public function testCanChangeMail(): void {
		$this->assertTrue($this->setting->canChangeMail());
	}

	public function testIsDefaultEnabledMail(): void {
		$this->assertFalse($this->setting->isDefaultEnabledMail());
	}
}
