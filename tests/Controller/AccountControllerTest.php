<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Controller;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCA\DropAccount\Controller\AccountController;
use OCA\DropAccount\Service\ActivityService;
use OCA\DropAccount\Service\MailerService;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\BackgroundJob\IJobList;
use OCP\IAppConfig;
use OCP\IConfig;
use OCP\IL10N;
use OCP\IRequest;
use OCP\IURLGenerator;
use OCP\IUser;
use OCP\IUserSession;
use OCP\PreConditionNotMetException;
use PHPUnit\Framework\MockObject\MockObject;

class AccountControllerTest extends TestCase {
	/** @var IUserSession|MockObject */
	private $userSession;

	/** @var IJobList|MockObject */
	private $jobList;

	/** @var IConfig|MockObject */
	private $config;

	/** @var IAppConfig|MockObject */
	private $appConfig;

	/** @var ActivityService|(ActivityService&MockObject)|MockObject */
	private $activityService;

	/**
	 * @var IUser|MockObject
	 */
	private $user;
	private AccountController $controller;

	public const USER_EMAIL = 'user@email.com';

	public function setUp(): void {
		parent::setUp();
		$request = $this->createMock(IRequest::class);
		$l10n = $this->createMock(IL10N::class);
		$this->userSession = $this->createMock(IUserSession::class);
		$this->jobList = $this->createMock(IJobList::class);
		$this->config = $this->createMock(IConfig::class);
		$this->appConfig = $this->createMock(IAppConfig::class);
		$urlGenerator = $this->createMock(IURLGenerator::class);
		$mailerService = $this->createMock(MailerService::class);
		$this->activityService = $this->createMock(ActivityService::class);
		$this->user = $this->createMock(IUser::class);
		$this->user->method('getDisplayName')->willReturn('User Displayname 123');
		$this->user->method('getEMailAddress')->willReturn(self::USER_EMAIL);

		$l10n
			->method('t')
			->willReturnCallback(function ($string, $args) {
				return 'TRANSLATED: ' . vsprintf($string, $args);
			});

		$this->controller = new AccountController(
			Application::APP_NAME,
			$request,
			$this->userSession,
			$this->config,
			$this->appConfig,
			$this->jobList,
			$urlGenerator,
			$mailerService,
			$this->activityService
		);
	}

	/**
	 * @throws PreConditionNotMetException
	 */
	public function testConfirmWithoutUserSession(): void {
		$this->userSession->expects($this->once())->method('getUser')->willReturn(null);
		$response = $this->controller->confirm('some-token');

		$expectedResponse = new TemplateResponse(Application::APP_NAME, 'account-deleted', ['uid' => null, 'status' => 'not-found'], 'guest');
		$expectedResponse->setStatus(Http::STATUS_UNAUTHORIZED);
		$this->assertEquals($expectedResponse, $response);
	}

	/**
	 * @throws PreConditionNotMetException
	 */
	public function testConfirmWithBadToken(): void {
		$this->user->expects($this->once())
			->method('getUID')
			->with()
			->willReturn('userid');
		$this->userSession->expects($this->once())->method('getUser')->willReturn($this->user);
		$this->config->expects($this->once())->method('getUserValue')->with('userid', Application::APP_NAME, 'delete_token', null)->willReturn('something-else');
		$response = $this->controller->confirm('some-token');

		$expectedResponse = new TemplateResponse(Application::APP_NAME, 'account-deleted', ['uid' => 'userid', 'status' => 'invalid-token', 'rootURL' => ''], 'guest');
		$expectedResponse->setStatus(Http::STATUS_NOT_FOUND);
		$this->assertEquals($expectedResponse, $response);
	}

	/**
	 * @param string $delayPurge
	 * @dataProvider dataTestConfirmAccount
	 * @throws PreConditionNotMetException
	 */
	public function testConfirm(string $delayPurge): void {
		$this->user->expects($this->exactly($delayPurge === 'yes' ? 4 : 3))
			->method('getUID')
			->with()
			->willReturn('userid');
		$this->userSession->expects($this->once())->method('getUser')->willReturn($this->user);
		$this->config->expects($this->once())->method('getUserValue')->with('userid', Application::APP_NAME, 'delete_token', null)->willReturn('some-token');

		$this->appConfig
			->expects($this->once())
			->method('getValueString')
			->with(Application::APP_NAME, 'delayPurge', 'no')
			->willReturn($delayPurge);

		$this->user->expects($this->once())
			->method('setEnabled')
			->with(false);

		if ($delayPurge === 'yes') {
			$this->config
				->expects($this->exactly(2))
				->method('setUserValue');
			$this->activityService->expects($this->once())
				->method('createAdminActivities');
			$this->activityService->expects($this->once())
				->method('sendActivities');
		} else {
			$this->jobList->expects($this->once())
				->method('add')
				->with(DeleteAccountData::class, ['uid' => 'userid']);
			$this->activityService->expects($this->never())
				->method('createAdminActivities');
			$this->activityService->expects($this->never())
				->method('sendActivities');
		}

		$response = $this->controller->confirm('some-token');

		$expectedResponse = new TemplateResponse(Application::APP_NAME, 'account-deleted', ['uid' => 'userid', 'status' => 'deleted'], 'guest');
		$this->assertEquals($expectedResponse, $response);
	}

	public function dataTestConfirmAccount(): array {
		return [
			[
				'no'
			],
			[
				'yes'
			]
		];
	}
}
