<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Settings;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Service\DeletionCapabilityService;
use OCA\DropAccount\Settings\Personal;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\IAppConfig;
use OCP\IUser;
use OCP\IUserSession;
use PHPUnit\Framework\MockObject\MockObject;

class PersonalTest extends TestCase {
	private Personal $personal;
	/**
	 * @var DeletionCapabilityService|(DeletionCapabilityService&MockObject)|MockObject
	 */
	private $deletionCapabilityService;
	/**
	 * @var IUserSession|MockObject
	 */
	private $userSession;

	/**
	 * @var IInitialState|MockObject
	 */
	private $initialState;
	/**
	 * @var IAppConfig|MockObject
	 */
	private $appConfig;

	public function setUp(): void {
		parent::setUp();
		$this->userSession = $this->createMock(IUserSession::class);
		$this->deletionCapabilityService = $this->createMock(DeletionCapabilityService::class);
		$this->appConfig = $this->createMock(IAppConfig::class);
		$this->initialState = $this->createMock(IInitialState::class);

		$this->personal = new Personal($this->appConfig, $this->userSession, $this->deletionCapabilityService, $this->initialState);
	}

	public function testGetPriority(): void {
		$this->assertEquals(40, $this->personal->getPriority());
	}

	public function testGetSection(): void {
		$this->assertEquals(Application::APP_NAME, $this->personal->getSection());
	}

	/**
	 * @dataProvider dataForTestGetForm
	 */
	public function testGetForm(bool $canBeDeleted, ?string $reason, string $uid, bool $isAdmin, bool $requiresConfirmation, ?string $email, array $result): void {
		$res = new TemplateResponse(Application::APP_NAME, 'personal');

		$this->deletionCapabilityService->expects($this->once())->method('canUserBeDeleted')->willReturn([$canBeDeleted, $reason]);

		$user = $this->createMock(IUser::class);
		$this->userSession->expects($this->once())->method('getUser')->willReturn($user);

		$matcher = $this->exactly(3);
		$this->appConfig->expects($matcher)->method('getValueString')->willReturnCallback(function ($appId, $key, $value) use ($matcher, $requiresConfirmation): string {
			$result = [$requiresConfirmation ? 'yes' : 'no', 'no', '24'];
			match ($matcher->numberOfInvocations()) {
				1 => $this->assertEquals('requireConfirmation', $key),
				2 => $this->assertEquals('delayPurge', $key),
				3 => $this->assertEquals('delayPurgeHours', $key),
			};
			return $result[$matcher->numberOfInvocations() - 1];
		});

		if ($requiresConfirmation) {
			$user->expects($this->exactly($email === null ? 1 : 2))->method('getEMailAddress')->willReturn($email);
		}

		$matcher = $this->exactly(6);
		$this->initialState->expects($matcher)
			->method('provideInitialState')
			->willReturnCallback(function (string $key, $data) use ($matcher, $result): void {
				$keys = [
					'has_email_for_confirmation',
					'can_delete_account',
					'deletion_impossible_reason',
					'will_delay_purge',
					'delay_purge_hours',
					'require_confirmation'
				];
				$count = $matcher->numberOfInvocations();
				$this->assertEquals($result[$key], $data);
			});

		$this->assertEquals($res, $this->personal->getForm());
	}

	public static function dataForTestGetForm(): array {
		return [
			[
				true, null, 'bye', true, false, null, ['can_delete_account' => true, 'deletion_impossible_reason' => null, 'has_email_for_confirmation' => false, 'require_confirmation' => false, 'will_delay_purge' => false, 'delay_purge_hours' => 24]
			],
			[
				false, 'some reason', 'bye', false, false, null, ['can_delete_account' => false, 'deletion_impossible_reason' => 'some reason', 'has_email_for_confirmation' => false, 'require_confirmation' => false, 'will_delay_purge' => false, 'delay_purge_hours' => 24]
			],
			[
				true, null, 'bye', true, true, null, ['can_delete_account' => true, 'deletion_impossible_reason' => null, 'has_email_for_confirmation' => false, 'require_confirmation' => true, 'will_delay_purge' => false, 'delay_purge_hours' => 24]
			],
			[
				true, null, 'bye', true, true, '', ['can_delete_account' => true, 'deletion_impossible_reason' => null, 'has_email_for_confirmation' => false, 'require_confirmation' => true, 'will_delay_purge' => false, 'delay_purge_hours' => 24]
			],
			[
				true, null, 'bye', true, true, 'toto@me.com', ['can_delete_account' => true, 'deletion_impossible_reason' => null, 'has_email_for_confirmation' => true, 'require_confirmation' => true, 'will_delay_purge' => false, 'delay_purge_hours' => 24]
			]
		];
	}
}
