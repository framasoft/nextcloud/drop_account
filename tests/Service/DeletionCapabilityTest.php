<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Service;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\Service\DeletionCapabilityService;
use OCP\IGroup;
use OCP\IGroupManager;
use OCP\IL10N;
use OCP\IUser;
use OCP\IUserManager;
use PHPUnit\Framework\MockObject\MockObject;

class DeletionCapabilityTest extends TestCase {

	/** @var IUserManager|(IUserManager&MockObject)|MockObject */
	private $userManager;

	/** @var IGroupManager|(IGroupManager&MockObject)|MockObject */
	private $groupManager;

	private DeletionCapabilityService $deletionCapabilityService;

	protected function setUp(): void {
		parent::setUp();
		$l10n = $this->createMock(IL10N::class);
		$l10n
			->method('t')
			->willReturnCallback(function ($string, $args) {
				return 'TRANSLATED: ' . vsprintf($string, $args);
			});
		$this->userManager = $this->createMock(IUserManager::class);
		$this->groupManager = $this->createMock(IGroupManager::class);
		$this->deletionCapabilityService = new DeletionCapabilityService($this->userManager, $this->groupManager, $l10n);
	}


	/**
	 * @dataProvider dataForTestCapabilitiesWithUser
	 */
	public function testCapabilitiesWithUser(string $backendClassName, bool $enabled, ?string $reason) {
		$user = $this->createMock(IUser::class);
		$user->expects($this->once())->method('getBackendClassName')->willReturn($backendClassName);

		$this->userManager->expects($enabled ? $this->once() : $this->never())->method('countUsers')->willReturn(2);
		$admin = $this->createMock(IGroup::class);
		$admin->expects($enabled ? $this->once() : $this->never())->method('count')->willReturn(1);
		$this->groupManager->expects($enabled ? $this->once() : $this->never())->method('get')->willReturn($admin);
		$user->expects($enabled ? $this->once() : $this->never())->method('getUID')->willReturn('someuser');
		$this->groupManager->expects($enabled ? $this->once() : $this->never())->method('isAdmin')->with('someuser')->willReturn(false);

		$this->assertEquals([$enabled, $reason], $this->deletionCapabilityService->canUserBeDeleted($user));
	}

	public function dataForTestCapabilitiesWithUser(): array {
		return [
			['Database', true, null],
			['Guests', true, null],
			['user_saml', false, 'TRANSLATED: User backend configuration does not allow deletion'],
			['LDAP', false, 'TRANSLATED: User backend configuration does not allow deletion'],
		];
	}

	/**
	 * @dataProvider dataForTestCapabilitiesWithUserConditions
	 */
	public function testCapabilitiesWithUserConditions(int $nbUsers, int $nbAdmins, bool $isAdmin, bool $enabled, ?string $reason) {
		$user = $this->createMock(IUser::class);
		$user->expects($this->once())->method('getBackendClassName')->willReturn('Database');

		$this->userManager->expects($this->once())->method('countUsers')->willReturn($nbUsers);
		$admin = $this->createMock(IGroup::class);
		$admin->expects($nbUsers > 1 ? $this->once() : $this->never())->method('count')->willReturn($nbAdmins);
		$this->groupManager->expects($nbUsers > 1 ? $this->once() : $this->never())->method('get')->willReturn($admin);
		$user->expects($nbUsers > 1 && $nbAdmins < 2 ? $this->once() : $this->never())->method('getUID')->willReturn('someuser');
		$this->groupManager->expects($nbUsers > 1 && $nbAdmins < 2 ? $this->once() : $this->never())->method('isAdmin')->with('someuser')->willReturn($isAdmin);

		$this->assertEquals([$enabled, $reason], $this->deletionCapabilityService->canUserBeDeleted($user));
	}

	public function dataForTestCapabilitiesWithUserConditions(): array {
		return [
			[1, 3, false, false, "TRANSLATED: You are the only user on the server, you can't delete your account."],
			[2, 1, false, true, null],
			[2, 1, true, false, "TRANSLATED: You are the only admin on the server, you can't delete your account."],
			[2, 2, true, true, null],
		];
	}
}
