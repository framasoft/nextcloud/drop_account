<?php

/*
 * SPDX-FileCopyrightText: 2017 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

return [
	'routes' => [
		['name' => 'account#confirm', 'url' => '/confirm/{token}', 'verb' => 'GET'],
	],
	'ocs' => [
		['name' => 'API#delete', 'url' => '/api/v1/account', 'verb' => 'DELETE']
	]
];
